from flask import Flask, abort, Response
from flask import request
import requests, csv
import pandas as pd


Token = 'c7d0427a11f6a857416094fd67d229a2379e3f91'

app = Flask(__name__)


@app.route('/webhook', methods=['POST'])
def func():
    if request.method == "POST":
        r = request.get_json()
        try:
            annotation = len(r.get('annotation'))
        except IndexError:
            annotation = 0
        except TypeError:
            annotation = 0
        try:
            predictions = len(r.get('predictions'))
        except IndexError:
            predictions = 0
        except TypeError:
            predictions = 0
        try:
            lead_time = r.get('annotation').get('lead_time')
        except IndexError:
            lead_time = 0.0
        except AttributeError:
            lead_time = 0.0
        dif = annotation - predictions
        ident = r.get('annotation').get('id')
        with open('file.csv', 'a') as file:
            writer = csv.writer(file)
            writer.writerow([annotation, predictions, lead_time, ident, dif])
        return Response(status=200)
    else:
        abort(400)


if __name__ == '__main__':
    app.run() 
